# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Queries ##
Una consulta que devuelva todos los datos de un incidente, y cuya clave de búsqueda sea...
- Id del incidente
- Categoría
- Día de la semana
- Día - Rango de días
- Hora - Rango de horas
- Distrito
- Resolución
- Dirección

Consultas por día de la semana:
- En qué día de la semana se cometen más delitos:: Día de la semana
- En qué día de la semana hay más incidentes:: Día de la semana
- Número de incidentes que han ocurrido un día de la semana:: Día de la semana, número

Consultas por categoría y fecha:
- Devolver los incidentes de una determinada categoría en una determinada fecha:: Id incidente, Descripción, Distrito, Calle

Consultas por rango de hora:
- Devolver los incidentes de un determinado rango de horas:: Id incidente, Descripción, Distrito, Calle

Consultas por categoría y resolución:
- Devolver los incidentes de una determinada categoría que han terminado con cierta resolución:: Id incidente, Descripción, Fecha