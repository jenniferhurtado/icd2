from cassandra.cluster import Cluster
import plotly.offline as offline
import plotly.graph_objs as go

cluster = Cluster();
session = None;
INCIDENTS_BY_INCIDNTNUM = 'SELECT category, descript, dayofweek, date, time, pddistrict, resolution, address FROM incidents_by_incidntnum WHERE incidntnum=';
INCIDENTS_BY_DATE1 = 'SELECT descript, pddistrict FROM incidents_by_date WHERE date=';
INCIDENTS_BY_DATE2 = ' AND category=';
INCIDENTS_BY_TIME = 'SELECT incidntnum, descript, pddistrict, address FROM incidents_by_time WHERE time=';
INCIDENTS_BY_CATEGORY1 = 'SELECT incidntnum, descript, date FROM incidents_by_category WHERE category=';
INCIDENTS_BY_CATEGORY2 = ' AND resolution=';
INCIDENTS_BY_DAYOFWEEK = 'SELECT incidntnum, category FROM incidents_by_dayofweek WHERE dayofweek='

def init(keyspace):
	global session;
	session = cluster.connect(keyspace);


def incidents_by_incidntnum(incidntnum):
	rows = session.execute(INCIDENTS_BY_INCIDNTNUM + incidntnum);
	for row in rows:
		print(row.category, '|', row.descript, '|',row.dayofweek,'|', row.date,'|', row.time,'|', row.pddistrict,'|', row.resolution,'|', row.address);


def incidents_by_date(date, category):
	rows = session.execute(INCIDENTS_BY_DATE1 + date + INCIDENTS_BY_DATE2 + category);
	for row in rows:
		print(row.descript, '|', row.pddistrict);


def incidents_by_time(time):
	rows = session.execute(INCIDENTS_BY_TIME + time);
	for row in rows:
		print(row.incidntnum, '|', row.descript, '|', row.pddistrict, '|', row.address);

def incidents_by_category(category, resolution):
	rows = session.execute(INCIDENTS_BY_CATEGORY1 + category + INCIDENTS_BY_CATEGORY2 + resolution);
	for row in rows:
		print(row.incidntnum, '|', row.descript, '|', row.date);

def incidents_by_dayofweek(dayofweek):
	rows = session.execute(INCIDENTS_BY_DAYOFWEEK + dayofweek);
	for row in rows:
		print(row.incidntnum, '|', row.category);
    

def print_dayofweek_count():
	rows = session.execute('SELECT incidntnum, dayofweek FROM incidents_by_dayofweek');
	l = [];
	for row in rows:
		l.append(row.dayofweek);

	myset = set(l);
	t = [];
	for x in myset:
		print(x, ": ", l.count(x));
		t.append(l.count(x));

	data = [go.Pie(labels=list(myset), values=t)]
	offline.plot(data, filename='dayofweek.html', image='')


def print_category_count():
	rows = session.execute('SELECT incidntnum, category FROM incidents_by_category');
	l = [];
	for row in rows:
		l.append(row.category);

	myset = set(l);
	t = [];
	for x in myset:
		print(x, ": ", l.count(x));
		t.append(l.count(x));

	data = [go.Bar(x=list(myset), y=t)]
	offline.plot(data, filename='category.html', image='')