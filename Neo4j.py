from neo4j.v1 import GraphDatabase
import visualise_data
class Neo4j:

    #Definicion de la conexion Neo4j
    def __init__(self):
        self._driver = GraphDatabase.driver("bolt://localhost:7687", auth=("neo4j", "prueba"))

    #cierre de la conexion Neo4j
    def close(self):
        self._driver.close()

    #query 1: obtener categoria de un numero de incidente
    def query1(self, numIncidente):
        print("Consulta numero 1:")
        with self._driver.session() as session:
            categorias = session.write_transaction(self._query1, numIncidente)
            mensaje="Incidente con numero " +str(numIncidente)+" pertenece a la categoria: "
            for c in  categorias:
                mensaje = mensaje+" ,"
                mensaje = mensaje+ c.properties["Name"]
            print(mensaje+"\n")
            
    @staticmethod
    def _query1(tx, numIncidente):
        listaResultado = []
        for result in tx.run("MATCH (i:Incident {IncidntNum:$numInci})-[:CON_CATEGORIA]->(c:Category) RETURN c",numInci=numIncidente):
            listaResultado = listaResultado + [result["c"]]
        return listaResultado

    #query 2: obtener incidentes de una categoria
    def query2(self, categoria,numeroResultado):
        print("Consulta numero 2:")
        with self._driver.session() as session:
            incidentes = session.write_transaction(self._query2, categoria,numeroResultado)
            mensaje="nº"+ str(numeroResultado)+" Incidentes con categoria " +categoria 
            for i in  incidentes:
               mensaje = mensaje+" ,"
               mensaje = mensaje+str(i.properties["IncidntNum"])
            print(mensaje+"\n")
            
    @staticmethod
    def _query2(tx, categoria,numeroResultado):
        listaResultado = []
        for result in tx.run("MATCH (Incident)-[:CON_CATEGORIA]->(Category {Name:$categoria}) Return Incident LIMIT $numeroResultado",categoria=categoria,numeroResultado=numeroResultado):
            listaResultado = listaResultado + [result["Incident"]]
        return listaResultado
    #query 3: obtener los n incidentes en un distrito
    def query3(self, distrito,numeroResultado):
        print("Consulta numero 3:")
        with self._driver.session() as session:
            incidentes = session.write_transaction(self._query3, distrito,numeroResultado)
            mensaje="nº"+ str(numeroResultado)+" Incidentes ocurridos en distrito " +distrito 
            for i in  incidentes:
               mensaje = mensaje+" ,"
               mensaje = mensaje+str(i.properties["IncidntNum"])
            print(mensaje+"\n")
            
    @staticmethod
    def _query3(tx, distrito,numeroResultado):
        listaResultado = []
        for result in tx.run("MATCH (Incident)-[:OCURRIDO_ENDISTRITO]->(District {Name:$distrito}) Return Incident LIMIT $numeroResultado",distrito=distrito,numeroResultado=numeroResultado):
            listaResultado = listaResultado + [result["Incident"]]
        return listaResultado
    
    #query 4: obtener numero total de  incidentes en un dia de la semana
    def query4(self, diaSemana):
        print("Consulta numero 4:")
        with self._driver.session() as session:
            numIncident = session.write_transaction(self._query4, diaSemana)
            mensaje="Cantidad de incidentes ocurridos en el dia de la semana " +diaSemana 
            print(mensaje+": "+str(numIncident)+"\n")
            return numIncident
            
    @staticmethod
    def _query4(tx, diaSemana):
        result = tx.run("MATCH (i:Incident)-[:OCURRIDO_DIASEMANA]->(DayOfWeek {Name:$diaSemana}) Return COUNT(i) ",diaSemana=diaSemana)
        return result.single()[0]
    
    #query 5: obtener numero total de  incidentes de una categoria
    def query5(self, categoria):
        print("Consulta numero 5:")
        with self._driver.session() as session:
            numIncident = session.write_transaction(self._query5, categoria)
            mensaje="Cantidad de incidentes con categoria " +categoria 
            print(mensaje+": "+str(numIncident)+"\n")
            return numIncident
            
    @staticmethod
    def _query5(tx, categoria):
        result = tx.run("MATCH (Incident)-[:CON_CATEGORIA]->(Category {Name:$categoria}) Return Count(Incident)",categoria=categoria)
        return result.single()[0]
    
    #query 6: Devuelve los incidentes con el distrito y categoría definidos
    def query6(self, categoria, distrito):
        print("Consulta numero 6:")
        with self._driver.session() as session:
            incidentes = session.write_transaction(self._query6, categoria, distrito)
            mensaje="Incidentes con categoria " +categoria+" y distrito "+distrito 
            for i in incidentes:
               mensaje = mensaje+" ,"
               mensaje = mensaje+str(i.properties["IncidntNum"])
            print(mensaje+"\n")
            
    @staticmethod
    def _query6(tx, categoria, distrito):
        listaResultado = []
        for result in tx.run("MATCH (i:Incident)-[:CON_CATEGORIA]->(Category {Name:$categoria}), (i:Incident)-[:OCURRIDO_ENDISTRITO]->(District {Name:$distrito}) Return i",categoria=categoria, distrito=distrito):
            listaResultado = listaResultado + [result["i"]]
        return listaResultado
    
    #query 7: Devuelve los incidentes con la categoría y dia de la semana definidos
    def query7(self, categoria, diaSemana):
        print("Consulta numero 7:")
        with self._driver.session() as session:
            incidentes = session.write_transaction(self._query7, categoria, diaSemana)
            mensaje="Incidentes con categoria " +categoria+" y ocurrido en el dia de la semana "+diaSemana 
            for i in incidentes:
               mensaje = mensaje+" ,"
               mensaje = mensaje+str(i.properties["IncidntNum"])
            print(mensaje+"\n")
            
    @staticmethod
    def _query7(tx, categoria, diaSemana):
        listaResultado = []
        for result in tx.run("MATCH (i:Incident)-[:CON_CATEGORIA]->(Category {Name:$categoria}), (i:Incident)-[:OCURRIDO_DIASEMANA]->(DayOfWeek {Name:$diaSemana}) Return i",categoria=categoria, diaSemana=diaSemana):
            listaResultado = listaResultado + [result["i"]]
        return listaResultado
    
    #query 8: Devuelve los incidentes curridos en una direccion
    def query8(self, direccion,):
        print("Consulta numero 8:")
        with self._driver.session() as session:
            incidentes = session.write_transaction(self._query8, direccion)
            mensaje="Incidentes ocurridos en la direccion " +direccion
            for i in incidentes:
               mensaje = mensaje+" ,"
               mensaje = mensaje+str(i.properties["IncidntNum"])
            print(mensaje+"\n")
            
    @staticmethod
    def _query8(tx, direccion):
        listaResultado = []
        for result in tx.run("MATCH (i:Incident{Address:$direccion}) RETURN i",direccion=direccion):
            listaResultado = listaResultado + [result["i"]]
        return listaResultado
    
    #query 9: Devuelve los datos de un incidente
    def query9(self, numeroIncidente):
        print("Consulta numero 9:")
        with self._driver.session() as session:
            i = session.write_transaction(self._query9, numeroIncidente)
            mensaje="Datos del incidente " +str(numeroIncidente)+": "
            mensaje = mensaje+"Direccion: "+i.properties["Address"]+" ,Fecha: "+i.properties["Date"]+" ,Resolucion: "+i.properties["Resolution"]+" ,Hora: "+i.properties["Time"]
            print(mensaje+"\n")
            
    @staticmethod
    def _query9(tx, numeroIncidente):
        result =tx.run("MATCH (i:Incident{IncidntNum:$numeroIncidente}) RETURN i",numeroIncidente=numeroIncidente)
        return result.single()[0]
    
    #query 10: Devuelve los diferentes tipos de Categorias
    def query10(self):
        print("Consulta numero 10:")
        with self._driver.session() as session:
            categorias = session.write_transaction(self._query10)
            mensaje="Las categorias existentos son: "
            for c in categorias:
               mensaje = mensaje+" ,"
               mensaje = mensaje+c
            print(mensaje+"\n")
            return categorias
            
    @staticmethod
    def _query10(tx):
        listaResultado = []
        for result in tx.run("MATCH (n:Category) RETURN n.Name"):
            listaResultado = listaResultado + [result["n.Name"]]
        return listaResultado
    
    
    #query 11: obtener numero total de  incidentes en un distrito
    def query11(self, distrito):
        print("Consulta numero 11:")
        with self._driver.session() as session:
            numIncident = session.write_transaction(self._query11, distrito)
            mensaje="Cantidad de incidentes en un distrito " +distrito 
            print(mensaje+": "+str(numIncident)+"\n")
            return numIncident
            
    @staticmethod
    def _query11(tx, distrito):
        result = tx.run("MATCH (Incident)-[:OCURRIDO_ENDISTRITO]->(District {Name:$distrito}) Return Count(Incident)",distrito=distrito)
        return result.single()[0]
    
    #query 12: Devuelve los diferentes tipos de Categorias
    def query12(self):
        print("Consulta numero 12:")
        with self._driver.session() as session:
            distritos = session.write_transaction(self._query12)
            mensaje="Los distritos existentos son: "
            for d in distritos:
               mensaje = mensaje+" ,"
               mensaje = mensaje+d
            print(mensaje+"\n")
            return distritos
            
    @staticmethod
    def _query12(tx):
        listaResultado = []
        for result in tx.run("MATCH (n:District) RETURN n.Name"):
            listaResultado = listaResultado + [result["n.Name"]]
        return listaResultado
    
    #Devuelve el numero de incidentes de cada categoria
    def valoresCategorias(self,categorias):
        listaResultado = []
        for c in categorias:
            with self._driver.session() as session:
                numIncident = session.write_transaction(self._query5, c)
                listaResultado =  listaResultado + [numIncident]
        return listaResultado
    
    #Devuelve el numero de incidentes de cada distrito
    def valoresDistritos(self,distritos):
        listaResultado = []
        for d in distritos:
            with self._driver.session() as session:
                numIncident = session.write_transaction(self._query11, d)
                listaResultado =  listaResultado + [numIncident]
        return listaResultado
        
conexionBD = Neo4j()
conexionBD.query1(150098458)
conexionBD.query2("ASSAULT",30)
conexionBD.query3("TENDERLOIN",30)
numIncidentMonday = conexionBD.query4("Monday")
numIncidentTuesday = conexionBD.query4("Tuesday")
numIncidentWednesday = conexionBD.query4("Wednesday")
numIncidentThursday = conexionBD.query4("Thursday")
numIncidentFriday = conexionBD.query4("Friday")
numIncidentSaturday = conexionBD.query4("Saturday")
numIncidentSunday = conexionBD.query4("Sunday")
conexionBD.query5("ASSAULT")
conexionBD.query6("ASSAULT","TENDERLOIN")
conexionBD.query7("ASSAULT","Tuesday")
conexionBD.query8("700 Block of KIRKWOOD AV")
conexionBD.query9(150098458)
categorias=conexionBD.query10()
conexionBD.query11("TENDERLOIN")
distritos= conexionBD.query12()
valoresDistritos= conexionBD.valoresDistritos(distritos)
valoresCategorias=conexionBD.valoresCategorias(categorias)
visualise_data.pie_plot(distritos,valoresDistritos,'plotDistritos.html')
visualise_data.pie_plot(categorias,valoresCategorias,'plotCategoria.html')
EtiquetasSemana=['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
valoresSemana= [numIncidentMonday,numIncidentTuesday,numIncidentWednesday,numIncidentThursday,numIncidentFriday,numIncidentSaturday,numIncidentSunday]
visualise_data.bar_plot(EtiquetasSemana,valoresSemana,'plotSemana.html')
