#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <sstream>
#include "CSVIterator.cpp"

using namespace std;

int main()
{
	cout<<"Busqueda por id:\n"<<std::endl;
	CSVIterator* iterator = new CSVIterator("database.csv");
	vector <string> keys1 = {"IncidntNum"};
	vector <string> values1 = {"150060275"};
	iterator->getValuesByKeys(keys1, values1, "print");

	cout<<"--------------------------------------------------------------\n"<<std::endl;

	cout<<"Count por categoría y día de la semana:\n"<<std::endl;
	vector <string> keys2 = {"Category", "DayOfWeek"};
	vector <string> values2 = {"ROBBERY", "Sunday"};
	iterator->getValuesByKeys(keys2, values2, "count");

	cout<<"--------------------------------------------------------------\n"<<std::endl;

	cout<<"Busqueda por id indicando las columnas deseadas en salida:\n"<<std::endl;
	vector <string> keys3 = {"IncidntNum",};
	vector <string> values3 = {"150098226"};
	vector <string> out3 = {"IncidntNum", "Category", "PdDistrict"};
	iterator->getValuesByKeys(keys3, values3, "print", out3);

	cout<<"--------------------------------------------------------------\n"<<std::endl;

	cout<<"Busqueda de delitos en un distrito un día de la semana:\n"<<std::endl;
	vector <string> keys4 = {"PdDistrict", "DayOfWeek"};
	vector <string> values4 = {"TENDERLOIN", "Sunday"};
	vector <string> out4 = {"IncidntNum"};
	iterator->getValuesByKeys(keys4, values4, "print", out4);
	
	cout<<"--------------------------------------------------------------\n"<<std::endl;
	
	cout<<"Número de incidentes en una dirección un dia de la semana:\n"<<std::endl;
	vector <string> keys5 = {"Address", "DayOfWeek"};
	vector <string> values5 = {"300 Block of LEAVENWORTH ST", "Sunday"};
	iterator->getValuesByKeys(keys5, values5, "count");	

	return 0;
}
