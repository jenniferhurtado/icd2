#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <iterator>
#include <sstream>

class CSVIterator
{
public:
	CSVIterator(std::string _pathFile);
	~CSVIterator();
	template<typename Out>
	void split(const std::string &s, char delim, Out result);
	void getValuesByKeys(std::vector <std::string> keys, std::vector <std::string> values, std::string type, std::vector <std::string> out_fields);

private:
	void reOpen();
	int getResult(std::vector<std::string> elems, int outSize, std::string type, std::vector <std::string> out_fields, int count);
	int findValuesInRow(std::vector <std::string> keys, std::vector <std::string> values, std::vector<std::string> elems, int keySize);
	void printFinalCount(std::vector <std::string> keys, std::vector <std::string> values, int count);

	std::vector <std::string> headers;
	std::string pathFile;
	std::ifstream file;
};


