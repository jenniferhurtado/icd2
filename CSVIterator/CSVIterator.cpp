#include "CSVIterator.h"
#include <sstream>
#include <string>
#include <iterator>
#include <vector>
#include <algorithm>

CSVIterator::CSVIterator(std::string _pathFile)
{
	pathFile = _pathFile;
	
	std::string all_headers, header;
	reOpen();

	// The first line of the file contains the headers
	getline(file, all_headers);
	std::istringstream stream(all_headers);

	// The different headers are separated by';'
	// so we split and store them
	while (stream.good())
	{
		std::getline(stream, header, ';');
		headers.push_back(header);
	}
	
	/*int a = headers.size();
	for(int i = 0; i < a; i++) {
    		std::cout << headers[i] << std::endl;
	}*/
}
void CSVIterator::reOpen()
{
	file.close();
	file.open(pathFile);
}

//This function takes a line of the European CSV, and stores each value individually in 'result'
template<typename Out>
void CSVIterator::split(const std::string &s, char delim, Out result) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

// This is the main function, that goes over the CSV and finds the rows that match the pairs keys-values specified by the parameters
void CSVIterator::getValuesByKeys(std::vector <std::string> keys, std::vector <std::string> values, std::string type, std::vector <std::string> out_fields = {"ALL"})
{
	reOpen();

	/* The first line of the file contains the headers.
	   We don't need to process them anymore, sice we 
	   did that in the constructor, so we will just 
	   ignore the first line. */ 
	std::string line;
	getline(file, line);

	int keySize = keys.size();
	int outSize = out_fields.size();
	int count = 0;

	while (file.good())
	{	

		// 'elems' will store the values of the current row individually (splitted)
		std::vector<std::string> elems;

		// We take a new line and split it
		getline(file, line);	
		split(line, ';', std::back_inserter(elems));
		

		/* 'success' variable will store how many values of the current row
	    match the ones we are looking for */
		int success = findValuesInRow(keys, values, elems, keySize);

		/* If the number of matches is equal to the number of values we are looking for,
		   said row must be returned to the user as part of the query's result */
		if(success == keySize)
		{
			count = getResult(elems, outSize, type, out_fields, count);
		}
	}
	if (type == "count")
	{ 
		printFinalCount(keys, values, count);
	}
}


int CSVIterator::findValuesInRow(std::vector <std::string> keys, std::vector <std::string> values, std::vector<std::string> elems, int keySize)
{
	int success = 0;	

	for(int i = 0; i < keySize; i++)
	{	
		// We calculate the index of the key we are looking for, in our 'headers' array
		std::vector<std::string>::iterator it = find (headers.begin(), headers.end(), keys[i]);
		std::ptrdiff_t pos = std::distance(headers.begin(), it);

		// If the value said key have on the current row matches the value we were looking
		// for, then it's a hit
		if(values[i] == elems[pos])
		{
			++success;
		}
	}

	return success;
}


int CSVIterator::getResult(std::vector<std::string> elems, int outSize, std::string type, std::vector <std::string> out_fields, int count)
{
	if(type == "print")
	{	
		if(out_fields[0] == "ALL")
		{
			for (auto ii = elems.begin(); ii != elems.end(); ++ii)
					std::cout << '|' << *ii;
			std::cout<<"\n"<<std::endl;
		}else
		{	
			for(int i = 0; i < outSize; i++)
			{	
				std::vector<std::string>::iterator it = find (headers.begin(), headers.end(), out_fields[i]);
				std::ptrdiff_t pos = std::distance(headers.begin(), it);
				std::cout << '|' << elems[pos];
			}
			std::cout<<"|\n"<<std::endl;
		}
	}else if (type == "count") ++count;

	return count;
}

void CSVIterator::printFinalCount(std::vector <std::string> keys, std::vector <std::string> values, int count)
{
	std::cout<<"Count query to ";
	int a = keys.size();
	for(int i = 0; i < a; i++) {
			std::cout << keys[i]<<"="<<values[i]<<" ";
	}
	std::cout<<": "<<count<<"\n"<<std::endl;
}

CSVIterator::~CSVIterator()
{
}


