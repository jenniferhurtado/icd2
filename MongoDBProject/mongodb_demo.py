import datetime
# !/usr/bin/env python
import json

import pandas as pd
import progressbar
from bson.code import Code
from pymongo import MongoClient, GEO2D

STEP = 200
MORNING_START = datetime.datetime(9999, 1, 1, 7, 0)
MORNING_END = datetime.datetime(9999, 1, 1, 11, 59)
AFTERNOON_START = datetime.datetime(9999, 1, 1, 12, 0)
AFTERNOON_END = datetime.datetime(9999, 1, 1, 20, 59)
NIGHT_START = datetime.datetime(9999, 1, 1, 21, 0)
NIGHT_END = datetime.datetime(9999, 1, 1, 6, 59)


def create_database(client, db_name):
    # Creates the database for the San Francisco city incidents data
    db = client[db_name]
    return db


def create_collection(db, collection_name):
    # Creates the collection of the documents of that will represent the incidents
    collection = db[collection_name]

    # Creates an index that is going to allow us to do spacial searches
    collection.create_index([("Location", GEO2D)])

    return collection


def import_content(collection, path_to_file):
    # Reads the csv file into python's dataframe type
    csv = pd.read_csv(path_to_file)

    # We read the csv little by little
    bar = progressbar.ProgressBar()
    for start in bar(range(0, len(csv.index), STEP)):
        partial_csv = csv.ix[start:(start + STEP), :]

        # Reads the dataframe as json using orient=records,
        # https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_json.html
        csv_to_json = json.loads(partial_csv.to_json(orient='records'))

        # Dates and locations need to be preprocessed in order to MongoDB to consider this data like actual
        # dates and locations instead of simple strings or integers
        csv_to_json = parse_dates(csv_to_json)
        csv_to_json = parse_locations(csv_to_json)

        # We bulk all data in
        collection.insert(csv_to_json)

        # we free some memory as csv is not needed anymore ;)
        del partial_csv
        del csv_to_json


def find_one_incident(collection, key, value):
    return collection.find_one({key: value})


def find_one_incident_not_equal(collection, key, value):
    return collection.find_one({key: {"$ne": value}})


def find_several_incidents(collection, key, value):
    return collection.find({key: value})


def find_several_incidents_not_equal(collection, key, value):
    return collection.find({key: {"$ne": value}})


def find_by_several_pairs(collection, tuples):
    dictionary = {}
    for (key, value, option) in tuples:
        dictionary[key] = {option: value}
    return collection.find(dictionary)


def count_by_key(collection, key, condition):
    mapf = Code("function (){ if ("+condition+") { emit(this."+key+", 1) };};")
    reduce = Code("function (key, values) {"
                  "  var total = 0;"
                  "  for (var i = 0; i < values.length; i++) {"
                  "    total += values[i];"
                  "  }"
                  "  return total;"
                  "};")
    result = collection.map_reduce(mapf, reduce, 'results')
    return result


def find_max(map_reduce_result):
    max_value = 0
    max_key = None
    for document in map_reduce_result.find():
        if document['value'] > max_value:
            max_value = document['value']
            max_key = document['_id']
    return max_key, max_value


def filter_by_date(collection, key, start, end):
    return collection.find({key: {'$gte': start, '$lt': end}})


def filter_by_location(collection, x, y):
    return collection.find({"Location": {"$near": [x, y]}})


def parse_dates(csv_to_json):
    data = []
    for row in csv_to_json:
        date = row['Date'].split(' ')[0]
        row['Date'] = datetime.datetime.strptime(date, '%m/%d/%Y')
        hour = int(row['Time'].split(':')[0])
        minutes = int(row['Time'].split(':')[1])
        row['Time'] = datetime.datetime(9999, 1, 1, hour, minutes)
        row['TimePeriod'] = get_time_period(row['Time'])
        data.append(row)
    return data


def get_time_period(time):
    if MORNING_START <= time <= MORNING_END:
        period = 'Morning'
    elif AFTERNOON_START <= time <= AFTERNOON_END:
        period = 'Afternoon'
    else:
        period = 'Night'
    return period


def parse_locations(csv_to_json):
    data = []
    for row in csv_to_json:
        x = row['X']
        y = row['Y']
        row['Location'] = [x, y]
        data.append(row)
    return data


if __name__ == "__main__":
    # We could, get the path to the file containing the dataset by asking the user,
    # but for testing purposes we have just written it in the code

    # print('Please provide the full path to the dataset')
    # path_to_file = input()
    path_to_file = 'C:\\Users\Jennifer\Downloads\Incidents.csv'

    db_name = 'datascience'
    collection_name = 'incidents'

    # Connection to the database
    client = MongoClient('localhost', 27017)

    # We get or create the database
    db = create_database(client, db_name)

    # Creates or gets the collection of the documents of that represents the incidents
    collection = create_collection(db, collection_name)
    # import_content(collection, path_to_file)

    """
    Queries
    """

    """
    Consulta que devuelva todos los datos de un incidente, y cuya clave de búsqueda sea cualquier atributo
    """
    # We use find_one_incident(collection, key, value)
    q1 = find_one_incident(collection, 'IncidntNum', 150060275)
    q2 = find_one_incident(collection, 'Category', 'ROBBERY')

    print('\nQuery 1. Search for one incident by any key. Results:\n{}\n{}'.format(q1, q2))

    """
    Consultas por día de la semana:
     - En qué día de la semana hay más incidentes
     - En qué día de la semana se cometen más delitos
     - Número de incidentes que han ocurrido en un día de la semana
     - Número de incidentes que han ocurrido en un periodo de tiempo
    """
    # We use count_by_key(collection, key, condition)
    max_key1, max_value1 = find_max(count_by_key(collection, 'DayOfWeek', 'true'))

    print('\nQuery 2a. Day of the week with the most incidents. Results:\n{}: {}\n'.format(max_key1, max_value1))

    max_key2, max_value2 = find_max(count_by_key(collection, 'DayOfWeek', 'this.Category!=\"NON-CRIMINAL\"'))

    print('\nQuery 2b. Day of the week with the most crimes. Results:\n{}: {}\n'.format(max_key2, max_value2))

    map_reduce = count_by_key(collection, 'DayOfWeek', 'true')

    print('\nQuery 2c. Number of incidents by day of the week. Results:\n')
    for document in map_reduce.find():
        print(document)

    map_reduce2 = count_by_key(collection, 'TimePeriod', 'true')

    print('\nQuery 2d. Number of incidents by period of time. Results:\n')
    for document in map_reduce2.find():
        print(document)

    """
    Devolver los incidentes de una determinada categoría en una determinada fecha
    """
    # We use find_by_several_pairs(collection, parameters)
    start_date = datetime.datetime(2018, 2, 1)
    end_date = datetime.datetime(2018, 2, 2)

    parameters = [('Category', 'ROBBERY', '$eq'), ('Date', start_date, '$gte'), ('Date', end_date, '$lt')]
    query = find_by_several_pairs(collection,  parameters).limit(3)

    print('\nQuery 3. Search for incidents for a category in a date range. Results:\n')
    for document in query:
        print(document)

    """
    Devolver los incidentes de un determinado rango de horas
    """
    # We use filter_by_date(collection, key, start, end)
    start_date = datetime.datetime(9999, 1, 1, 15, 40)
    end_date = datetime.datetime(9999, 1, 1, 15, 50)

    query = filter_by_date(collection, 'Time', start_date, end_date).limit(3)

    print('\nQuery 4. Search for incidents in a time range. Results:\n')
    for document in query:
        print(document)

    """
    Devolver los incidentes de una determinada categoría que han terminado con cierta resolución
    """
    # We use find_by_several_pairs(collection, parameters)
    by_category = collection.find({'Category': 'WARRANT'})
    parameters = [('Category', 'WARRANTS', '$eq'), ('Resolution', 'ARREST, BOOKED', '$eq')]
    query = find_by_several_pairs(collection,  parameters).limit(3)

    print('\nQuery 5. Search for incidents of a category and a resolution. Results:\n')
    for document in query:
        print(document)

    """
    Devolver los incidentes cercanos a unas coordenadas
    """
    # We use filter_by_location(collection, x, y)
    near_incidents = filter_by_location(collection, 122.4215816814, 137.761700718).limit(3)

    print('\nQuery 6. Search for incidents close to a location. Results:\n')
    for document in near_incidents:
        print(document)
