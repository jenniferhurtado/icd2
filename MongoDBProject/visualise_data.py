import plotly.offline as offline
import plotly.graph_objs as go


def line_plot(x, y, title='', html_filename='line_plot.html', image_format=''):
    """
    Function that generates a line plot. Example:

    line_plot([1, 2, 3, 5], [4, 2, 3, 4], 'Line plot', image_format='png')

    :param x: data that will be represented on X axis
    :type x: List of numbers
    :param y: data that will be represented on Y axis
    :type y: List of numbers
    :param title: Plot's title
    :type title: String
    :param html_filename: name of the html file generated
    :type html_filename: String
    :param image_format: name of the image generated
    :type image_format: String
    :return:
    """
    offline.plot({'data': [{'x': x, 'y': y}],
                  'layout': {'title': title,
                 'font': dict(size=16)}},
                 image=image_format,
                 filename=html_filename)


def scatter_plot(xs, ys, modes, names, html_filename='line_plot.html', image_format=''):
    """
    Function that generates a scatter plot. Example:

     scatter_plot([[1, 2, 3, 5], [8, 5, 4, 5]], [[4, 2, 3, 4], [1, 8, 2, 7]], ['marker', 'lines+marker'],
                 ['some data', 'other data'])

    :param xs: list of data that will be represented on X axis
    :type xs: List of lists of numbers
    :param ys: data that will be represented on Y axis
    :type ys: List os lists of numbers
    :param modes: Plots' style. Choose from 'marker', 'lines+markers', 'lines'
    :type modes: List of strings
    :param names: Name of the displaying data
    :type names: List of strings
    :param html_filename: name of the html file generated
    :type html_filename: String
    :param image_format: name of the image generated
    :type image_format: String
    :return:
    """
    # Create a trace
    data = _get_traces(xs, ys, modes, names)

    # Plot
    offline.plot(data, filename=html_filename, image=image_format)


def _get_traces(xs, ys, modes, names):
    traces = []
    if len(xs) == len(ys) == len(modes) == len(names):
        for i in range(0, len(xs)):
            trace = go.Scatter(x=xs[i], y=ys[i], mode=modes[i], name=names[i])
            traces.append(trace)
    else:
        print("ERROR: traces can't be created due to missing data\nData received\n----------\n")
        print("x: {} sets of data received\n"
              "y: {} sets of data received\n"
              "modes: {} sets of data received\n"
              "names: {} sets of data received".format(len(xs), len(ys), len(modes), len(names)))
    return traces


def bar_plot(x, y, html_filename='bar_plot.html', image_format=''):
    """
    Function that generates a bar plot. Example:

    bar_plot(['giraffes', 'orangutans', 'monkeys'], [20, 14, 23])

    :param x: data that will be represented on X axis
    :type x: List of numbers
    :param y: data that will be represented on Y axis
    :type y: List of numbers
    :param html_filename: name of the html file generated
    :type html_filename: String
    :param image_format: name of the image generated
    :type image_format: String
    :return:
    """
    data = [go.Bar(x=x, y=y)]

    offline.plot(data, filename=html_filename, image=image_format)


def pie_plot(labels, values, html_filename='pie_plot.html', image_format=''):
    """
    Function that generates a pie plot. Example:

    pie_plot(['Oxygen', 'Hydrogen', 'Carbon_Dioxide', 'Nitrogen'], [4500, 2500, 1053, 500])

    :param labels: labels of the data
    :type labels: List of strings
    :param values: values corresponding to the labels
    :type values: List of numbers
    :param html_filename: name of the html file generated
    :type html_filename: String
    :param image_format: name of the image generated
    :type image_format: String
    :return:
    """
    trace = go.Pie(labels=labels, values=values)

    offline.plot([trace], filename=html_filename, image=image_format)


if __name__ == "__main__":
    line_plot([1, 2, 3, 5], [4, 2, 3, 4], 'Line plot', image_format='png')
    bar_plot(['giraffes', 'orangutans', 'monkeys'], [20, 14, 23])
    pie_plot(['Oxygen', 'Hydrogen', 'Carbon_Dioxide', 'Nitrogen'], [4500, 2500, 1053, 500])
    scatter_plot([[1, 2, 3, 5], [8, 5, 4, 5]], [[4, 2, 3, 4], [1, 8, 2, 7]], ['marker', 'lines+marker'],
                 ['some data', 'other data'])
