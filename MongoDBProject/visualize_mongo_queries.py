from mongodb_demo import *
from visualise_data import *
from datetime import datetime, timedelta
from dateutil import rrule


def print_incidents_per_day(collection):
    map_reduce = count_by_key(collection, 'DayOfWeek', 'true')
    days = []
    number_of_incidents = []
    for document in map_reduce.find():
        days.append(document['_id'])
        number_of_incidents.append(document['value'])

    pie_plot(days, number_of_incidents)
    bar_plot(days, number_of_incidents)


def print_incidents_per_period(collection):
    map_reduce2 = count_by_key(collection, 'TimePeriod', 'true')
    period_of_time = []
    number_of_incidents = []
    for document in map_reduce2.find():
        period_of_time.append(document['_id'])
        number_of_incidents.append(document['value'])

    pie_plot(period_of_time, number_of_incidents)
    bar_plot(period_of_time, number_of_incidents)


def print_assaults_per_date(collection):
    start = datetime(2014, 1, 1)
    four_years_later = start + timedelta(days=365 * 4)

    months = []
    values = []
    for dt in rrule.rrule(rrule.MONTHLY, dtstart=start, until=four_years_later):
        parameters = [('Category', 'ASSAULT', '$eq'), ('Date', start, '$gte'), ('Date', dt, '$lt')]
        partial = find_by_several_pairs(collection, parameters).count()
        months.append(start)
        values.append(partial)
        start = dt

    bar_plot(months, values)


def print_proportion_assaults_per_date(collection):
    start = datetime(2014, 1, 1)
    four_years_later = start + timedelta(days=365 * 4)

    months = []
    values = []
    for dt in rrule.rrule(rrule.MONTHLY, dtstart=start, until=four_years_later):
        parameters = [('Category', 'ASSAULT', '$eq'), ('Date', start, '$gte'), ('Date', dt, '$lt')]
        partial = find_by_several_pairs(collection, parameters).count()
        parameters = [('Date', start, '$gte'), ('Date', dt, '$lt')]
        total = find_by_several_pairs(collection,  parameters).count()
        months.append(start)
        values.append(partial / total)
        start = dt

    line_plot(months, values)


if __name__ == "__main__":
    db_name = 'datascience'
    collection_name = 'incidents'

    # Connection to the database
    client = MongoClient('localhost', 27017)

    # We get or create the database
    db = create_database(client, db_name)

    # Creates or gets the collection of the documents of that represents the incidents
    collection_incidents = create_collection(db, collection_name)

    # Number of incidents per day of the week
    print_incidents_per_day(collection_incidents)

    # Number of incidents per period of time
    print_incidents_per_period(collection_incidents)

    # Number of incidents per month in 4 years
    print_assaults_per_date(collection_incidents)
    print_proportion_assaults_per_date(collection_incidents)
