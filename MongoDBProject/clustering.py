from sklearn.cluster import KMeans

from mongodb_demo import *


def parse_data():
    db_name = 'datascience'
    collection_name = 'incidents'

    # Connection to the database
    client = MongoClient('localhost', 27017)

    # We get or create the database
    db = create_database(client, db_name)

    # Creates or gets the collection of the documents of that represents the incidents
    collection = create_collection(db, collection_name)

    matrix = []
    for document in collection.find():
        category = parse_category(document['Category'])
        day = parse_day(document['DayOfWeek'])
        period = parse_period(document['TimePeriod'])
        district = parse_district(document['PdDistrict'])
        resolution = parse_resolution(document['Resolution'])
        matrix.append([category, day, period, district, resolution])

    return matrix


def parse_category(category):
    code = -1
    if category == 'LARCENY/THEFT':
        code = 0
    elif category == 'OTHER OFFENSES':
        code = 1
    elif category == 'NON-CRIMINAL':
        code = 2
    elif category == 'MISSING PERSON':
        code = 3
    elif category == 'VEHICLE THEFT':
        code = 4
    elif category == 'BURGLARY':
        code = 5
    elif category == 'VANDALISM':
        code = 6
    elif category == 'ASSAULT':
        code = 7
    elif category == 'PROSTITUTION':
        code = 8
    elif category == 'RECOVERED VEHICLE':
        code = 9
    elif category == 'ROBBERY':
        code = 10
    elif category == 'LIQUOR LAWS':
        code =11
    elif category == 'DRUG/NARCOTIC':
        code = 12
    elif category == 'FORGERY/COUNTERFEITING':
        code = 13
    elif category == 'SUSPICIOUS OCC':
        code = 14
    elif category == 'EMBEZZLEMENT':
        code = 15
    elif category == 'STOLEN PROPERTY':
        code = 16
    elif category == 'SECONDARY CODES':
        code = 17

    return code


def parse_day(day):
    code = -1
    if day == 'Monday':
        code = 0
    elif day == 'Tuesday':
        code = 1
    elif day == 'Wednesday':
        code = 2
    elif day == 'Thursday':
        code = 3
    elif day == 'Friday':
        code = 4
    elif day == 'Saturday':
        code = 5
    elif day == 'Sunday':
        code = 6

    return code


def parse_period(period):
    code = -1
    if period == 'Morning':
        code = 0
    elif period == 'Afternoon':
        code = 1
    elif period == 'Night':
        code = 2

    return code


def parse_district(district):
    code = -1
    if district == 'MISSION':
        code = 0
    elif district == 'SOUTHERN':
        code = 1
    elif district == 'INGLESIDE':
        code = 2
    elif district == 'BAYVIEW':
        code = 3
    elif district == 'TARAVAL':
        code = 4
    elif district == 'TENDERLOIN':
        code = 5
    elif district == 'CENTRAL':
        code = 6
    elif district == 'NORTHERN':
        code = 7
    elif district == 'RICHMOND':
        code = 8
    elif district == 'PARK':
        code = 9

    return code


def parse_resolution(resolution):
    code = -1
    if resolution == 'NONE':
        code = 0
    elif resolution == 'ARREST, BOOKED':
        code = 1
    elif resolution == 'ARREST, CITED':
        code = 2
    elif resolution == 'DISTRICT ATTORNEY REFUSES TO PROSECUTE':
        code = 3
    elif resolution == 'JUVENILE CITED':
        code = 4
    elif resolution == 'LOCATED':
        code = 5
    elif resolution == 'UNFOUNDED':
        code = 6

    return code


if __name__ == "__main__":
    print('Parsing data...')
    matrix = parse_data()
    print('Clustering')
    km = KMeans(n_clusters=9)
    fit_model = km.fit(matrix)

    # incident = NON-CRIMINAL, Monday, Afternoon, TENDERLOIN
    incident = [2, 0, 1, 5, -1]
    prediction = km.predict(matrix)
    print(km.predict([incident]))
